import * as _ from 'lodash';
import { EntityRepository, Repository } from 'typeorm';

import User from '../models/user.model';
import { UserFactory } from '../factory';
import Kingdom from '../models/kingdom.model';

@EntityRepository(User)
export default class UserRepository extends Repository<User> {
  /**
   * Checks to see if a given user already exists by the provided username or not.
   * @param username The username of the user being checked.
   */
  public async userExistsByUsername(username: string): Promise<boolean> {
    if (_.isNil(username)) return false;

    return (await User.count({ where: { username: username.toLowerCase() } })) >= 1;
  }

  /**
   * Attempts to find a user by a given username in the database, if the user does not exist then
   * the return promise value would just be null.
   * @param username the username of the user being found.
   */
  public getUserByUsername(username: string, relations: string[] = []): Promise<User> {
    if (_.isNil(username)) return null;

    username = username.toLowerCase();

    return User.findOne({ where: { username }, relations });
  }

  /**
   * Attempts to return a kingdom for a given user, if the kingdom does not exist then the response
   * will be null.
   * @param User The user who's kingdom is being located.
   */
  public async getUserKingdom(user: User): Promise<Kingdom> {
    return (await User.findOne({ select: ['id'], where: { id: user.id }, relations: ['kingdom'] })).kingdom;
  }

  public async createByUsername(username: string): Promise<User> {
    username = username.toLowerCase();

    const defaultUser = UserFactory.withUsername(username);
    const created = User.create(defaultUser);

    await User.save(created);
    return created;
  }
}
