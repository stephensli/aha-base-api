import * as Ratelimit from 'express-rate-limit';
import * as bodyParser from 'body-parser';
import * as express from 'express';
import * as cors from 'cors';
import * as http from 'http';

import * as Connection from './Connection.service';

import logger from '../libs/logger';

import { routes as Routes } from '../routes';

export default class ServerService {
  /**
   * Make the connection to the server based on the singleton connection object from the connection
   * database. We then attempt to sync the connection with the database models.
   */
  public static async ConnectToDatabase() {
    try {
      const connection = await Connection.Connection;
      await connection.query('select 1+1 as answer');
      await connection.synchronize();
    } catch (error) {
      logger.error(`Failed to connect to database, ${error}`);
    }
  }

  /**
   * The running express server that will be handling and processing all of the requests/routing.
   */

  private readonly app: express.Express;

  /**
   * The running http server listening for the requests performed by the users to the given
   * endpoints.
   */
  private readonly server: http.Server;

  constructor() {
    // the running express server.
    this.app = express();

    // the running http server.
    this.server = http.createServer(this.app);
  }

  /**
   * Starts the server up and running, configuring express and the router, after connecting the
   * database.
   */
  public async Start() {
    await ServerService.ConnectToDatabase();
    this.ExpressConfiguration();
    this.ConfigurationRouter();
    return this.server;
  }

  /**
   * Setup the express configurations, setting up the body parser. logging and cors. Sets a base
   * limit of 1mb of data for the adding / inserting of json body content.
   */
  public ExpressConfiguration() {
    this.app.use(new Ratelimit({ max: 100, windowMs: 1000 }));
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json({ limit: '1mb' }));
    this.app.use(cors());
  }

  /**
   * Configure and add all the routes to express, making sure that they are on the correct endpoints
   * paths, middleware and handler. This also includes the endpoint version.
   */
  public ConfigurationRouter() {
    const router = express.Router();

    // build up a router containing all the routes and information for the request, making sure that
    // we apply the path and the version.
    Routes.forEach((route) => {
      router.use(`/v${route.version}${route.path}`, route.handler);
    });

    // bind the router of all the routes and finally the missing handle, which will catch all routes
    // missed. Just allows for quick responses and makes the server feel like its not hanging.
    this.app.use('/api', router);
  }
}
