import { getCustomRepository } from 'typeorm';
import { Response, NextFunction } from 'express';
import * as _ from 'lodash';

import httpCodes from '../constants/httpCodes';
import UserRepository from '../repository/user.repository';
import { IUserRequest } from '../requests/IRequest';

export default class UserMiddleware {
  public static async bindUserByParamUsername(request: IUserRequest, response: Response, next: NextFunction) {
    const username = request.params.username;

    // ensure that we have a username to work from before attempting to continue. Otherwise we would
    // just be making a query based on nothing.
    if (_.isNil(username))
      return response.status(httpCodes.BAD_REQUEST).json({ message: 'no username parameter was provided.' });

    const userRepository = getCustomRepository(UserRepository);
    const user = await userRepository.getUserByUsername(username, ['kingdom']);

    // if the user does not exist, we cannot fullfil the complete request. SO lets go and let the user
    // know that the user does not exist and return out of the request.
    if (_.isNil(user))
      return response
        .status(httpCodes.NOT_FOUND)
        .json({ message: `a user does not exist by the provided username ${username}.` });

    request.user = user;
    next();
  }
}
