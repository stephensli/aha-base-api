import { Router } from 'express';

import userController from '../controllers/user.controller';
import kingdomController from '../controllers/kingdom.controller';
import InventoryController from '../controllers/inventory.controller';
import OutfitsController from '../controllers/outfit.controller';
import UserMiddleware from '../middleware/user.middleware';

import { asyncErrorHandler } from './handlers/asyncError.handler';

const userRouter = Router();

userRouter.post('/', asyncErrorHandler(userController.createNewUser));

userRouter.get(
  '/:username',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(userController.getUserByUsername)
);

userRouter.get(
  '/:username/kingdom',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(kingdomController.getUserKingdomByName)
);

userRouter.post(
  '/:username/kingdom',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(kingdomController.setUserKingdomByName)
);

/******************
 * BELOW HERE IS NOT IMPLEMENTED.
 */

userRouter.get(
  '/:username/inventory',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(InventoryController.getAllUserInventory)
);

userRouter.post(
  '/:username/inventory',
  [UserMiddleware.bindUserByParamUsername],
  InventoryController.addItemsToUserInventory
);

userRouter.get(
  '/:username/inventory/:identifier',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(InventoryController.getUserInventoryItemByIdentifier)
);

userRouter.delete(
  '/:username/inventory/:identifier',
  asyncErrorHandler(InventoryController.deleteUserInventoryItemByIdentifier)
);

userRouter.get(
  '/:username/outfits',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(OutfitsController.getAllUserOutfits)
);

userRouter.post(
  '/:username/outfits',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(OutfitsController.addOutfitsToUserOutfits)
);

userRouter.get(
  '/:username/outfits/:identifier',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(OutfitsController.getUserOutfitItemByIdentifier)
);

userRouter.delete(
  '/:username/outfits/:identifier',
  [UserMiddleware.bindUserByParamUsername],
  asyncErrorHandler(OutfitsController.deleteUserOutfitItemByIdentifier)
);

export default userRouter;
