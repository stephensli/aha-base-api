import { Router } from 'express';

import kingdomController from '../controllers/kingdom.controller';
import KingdomMiddleware from '../middleware/kingdom.middleware';

import { asyncErrorHandler } from './handlers/asyncError.handler';

const kingdomRouter = Router();

kingdomRouter.get('/', asyncErrorHandler(kingdomController.getAllKingdoms));
kingdomRouter.post('/', asyncErrorHandler(kingdomController.createNewKingdom));

kingdomRouter.get(
  '/:identifier',
  [KingdomMiddleware.bindKingdomByParamIdentifier],
  asyncErrorHandler(kingdomController.getKingdomByIdentifier)
);

kingdomRouter.patch(
  '/:identifier',
  [KingdomMiddleware.bindKingdomByParamIdentifier],
  asyncErrorHandler(kingdomController.updateKingdomByIdentifier)
);

kingdomRouter.delete(
  '/:identifier',
  [KingdomMiddleware.bindKingdomByParamIdentifier],
  asyncErrorHandler(kingdomController.deleteKingdomByIdentifier)
);

export default kingdomRouter;
