import * as express from 'express';

import infrastructureRouter from './infrastructure.router';
import loggingRouter from './logging.router';
import kingdomRouter from './kingdom.router';
import userRouter from './user.router';

interface IRouter {
  path: string;
  version: string;
  handler: express.Router;
}

export const routes: IRouter[] = [
  {
    handler: loggingRouter,
    version: '1.0',
    path: '/'
  },
  {
    handler: infrastructureRouter,
    version: '1.0',
    path: '/infrastructure'
  },
  {
    handler: userRouter,
    version: '1.0',
    path: '/users'
  },
  {
    handler: kingdomRouter,
    version: '1.0',
    path: '/kingdoms'
  }
];
