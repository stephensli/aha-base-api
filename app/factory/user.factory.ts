import User from '../models/user.model';

export default class UserFactory {
  public static default(): User {
    return new User();
  }

  public static withUsername(username: string): User {
    const user = this.default();

    user.username = username;
    user.twitchId = username;

    return user;
  }
}
