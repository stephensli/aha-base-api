import { Entity, Column } from 'typeorm';
import BaseModel from './base.model';

@Entity('transaction_type')
export default class TransactionType extends BaseModel {
  /**
   * The name of the type of transcation.
   */
  @Column({ type: 'text', name: 'transaction_type_name' })
  public name: string;
}
