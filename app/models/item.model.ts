import { Entity, Column, ManyToOne, JoinColumn } from 'typeorm';

import BaseModel from './base.model';
import ItemCategory from './itemCategory.model';

@Entity('item')
export default class Item extends BaseModel {
  /**
   * Name of the item
   */
  @Column({ unique: true, type: 'text', name: 'item_name' })
  public name: string;

  /**
   * Name of the item
   */
  @Column({ type: 'text', name: 'item_description' })
  public description: string;

  /**
   * PNG of the icon
   */
  @Column({ type: 'bytea', name: 'item_icon' })
  public icon: number;

  /**
   * The relational ship between the item and its cateogry.
   */
  @ManyToOne((type) => ItemCategory, (category) => category.id)
  @JoinColumn({ name: 'item_category_id' })
  public type: ItemCategory;
}
