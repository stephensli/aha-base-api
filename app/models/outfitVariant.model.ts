import { Entity, Column } from 'typeorm';
import BaseModel from './base.model';

@Entity('outfit_variant')
export default class OutfitVariant extends BaseModel {
  /**
   * The name of the item variant
   */
  @Column({ type: 'text', name: 'outfit_variant_name' })
  public name: string;
}
