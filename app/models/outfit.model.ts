import { Entity, Column, JoinColumn, ManyToOne } from 'typeorm';

import OutfitVariant from './outfitVariant.model';
import BaseModel from './base.model';

@Entity('outfit')
export default class Outfit extends BaseModel {
  /**
   * Name of the outfit
   */
  @Column({ type: 'text', name: 'outfit_name' })
  public name: string;

  /**
   * What kind of variant it is.
   */
  @Column({ type: 'text', name: 'outfit_variant_type' })
  public category: OutfitVariant;

  /**
   * PNG of the icon
   */
  @Column({ type: 'bytea', name: 'outfit_file' })
  public file: number;

  /**
   * The relational ship between the outfit and its variant.
   */
  @ManyToOne((type) => OutfitVariant, (variant) => variant.id)
  @JoinColumn({ name: 'outfit_variant_id' })
  public type: OutfitVariant;
}
