import { Entity, Column, Generated } from 'typeorm';
import BaseModel from './base.model';

@Entity('season')
export default class Season extends BaseModel {
  /**
   * The name of the given season.
   */
  @Column({ unique: true, type: 'text', name: 'season_name' })
  public name: string;

  /**
   * The incrementing seasion number.
   */
  @Generated('increment')
  @Column({ unique: true, type: 'int', name: 'season_number' })
  public number: number;

  /**
   * The start date of the seasion.
   */
  @Column({ type: 'date', name: 'season_start_date' })
  public startDate: Date;

  /**
   * The end date of the session.
   */
  @Column({ type: 'date', name: 'season_end_date' })
  public endDate: Date;
}
