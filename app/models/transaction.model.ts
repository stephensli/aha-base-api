import { Entity, JoinColumn, ManyToOne } from 'typeorm';
import BaseModel from './base.model';
import TransactionType from './transactionType.model';

@Entity('transaction')
export default class Transaction extends BaseModel {
  /**
   * The relational ship between the transaction and its given type.
   */
  @ManyToOne((type) => TransactionType, (type) => type.id)
  @JoinColumn({ name: 'transaction_type_id' })
  public type: TransactionType;
}
