import { Request } from 'express';

import User from '../models/user.model';
import Kingdom from '../models/kingdom.model';

/**
 * Extends the default express request to contain a localized object of the aha user, this will
 * be pushed on during the authentication process. And accessible if required.
 */
export interface IUserRequest extends Request {
  user: User;
}

/**
 * Extends the default express request to contain a localized object of the aha kingdom, this will
 * be pushed on during the authentication process. And accessible if required.
 */
export interface IKingdomRequest extends Request {
  kingdom: Kingdom;
}
