import { Request, Response } from 'express';
import * as _ from 'lodash';

import httpCodes from '../constants/httpCodes';
import { IUserRequest } from '../requests/IRequest';

export default class InventoryController {
  public static async getAllUserInventory(request: IUserRequest, response: Response) {}
  public static async getUserInventoryItemByIdentifier(request: IUserRequest, response: Response) {}
  public static async deleteUserInventoryItemByIdentifier(request: IUserRequest, response: Response) {}
  public static async addItemsToUserInventory(request: IUserRequest, response: Response) {}
}
