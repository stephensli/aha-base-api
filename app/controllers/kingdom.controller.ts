import { getCustomRepository } from 'typeorm';
import { Request, Response } from 'express';
import * as _ from 'lodash';

import { IUserRequest, IKingdomRequest } from '../requests/IRequest';
import KingdomRepository from '../repository/kingdom.repository';
import UserRepository from '../repository/user.repository';
import KingdomFactory from '../factory/kingdom.factory';
import httpCodes from '../constants/httpCodes';

export default class KingdomController {
  /**
   * Creates a new kingdom based on the name given in the post body. If the kingdom already exists
   * then the request will be rejected otherwise the new kingdom will be created and returned to the
   * user.
   */
  public static async createNewKingdom(request: Request, response: Response) {
    // Since a kingdom only has a name for it to be required to be created, lets pull the name from
    // the body and ensure that the kingdom does not already exist.
    const { name } = request.body;

    if (_.isNil(name) || name.length <= 4)
      return response.status(httpCodes.BAD_REQUEST).json({
        message: 'the provided kingdom name is invalid, empty or does not meet the length requirement.'
      });

    const kingdomRepository = getCustomRepository(KingdomRepository);

    if (await kingdomRepository.existsByIdentifer(name))
      return response.status(httpCodes.CONFLICT).json({
        message: `a kingdom already exists by the provided identifer: ${name}`
      });

    const kingdom = KingdomFactory.default();
    kingdom.name = name;

    await kingdom.save();
    return response.json({ kingdom });
  }

  /**
   * Gathers all kingdoms that are currently in the database and returns them as a array of kingdoms
   * to the current requesting user.
   */
  public static async getAllKingdoms(request: Request, response: Response) {
    const kingdomRepository = getCustomRepository(KingdomRepository);
    const kingdoms = await kingdomRepository.getAllKingdoms();

    return response.json({ kingdoms });
  }

  /**
   * Returns a given kingdom by the provided identifier. The identifier can be anything from the
   * name of the kingdom or the id of the kingdom. Both will ensure that a given kingdom is
   * returned.
   */
  public static async getKingdomByIdentifier(request: IKingdomRequest, response: Response) {
    // since the kingdom routes are being funneled through the middleware for binding a kingdom, we
    // don't have to check that the given kingdom exists. If it made it this far then the kingdom
    // would be on the request object.
    return response.json({ kingdom: request.kingdom });
  }

  /**
   * Used to update the properties of a given kingdom. Taking the already request bound kingdom,
   * updates its properties and then saves it again. Ensuring to return the newly updated kingdom.
   */
  public static async updateKingdomByIdentifier(request: IKingdomRequest, response: Response) {
    // Since a kingdom only has a name for it to be required to be created, lets pull the name from
    // the body and ensure that the kingdom does not already exist.
    const { name } = request.body;

    if (_.isNil(name) || name.length <= 4)
      return response.status(httpCodes.BAD_REQUEST).json({
        message: 'the provided kingdom name is invalid, empty or does not meet the length requirement.'
      });

    // since the request will go through the process of validating that the given kingdom already
    // exists, once it hits this location, the kingdom will already exist on the request so we can
    // update and then save it again.
    request.kingdom.name = name;
    await request.kingdom.save();

    return response.json({ kingdom: request.kingdom });
  }

  /**
   * Takes the request bound kingdom and removes it from the database. Ensuring to send a 200
   * response if everything went as planned.
   */
  public static async deleteKingdomByIdentifier(request: IKingdomRequest, response: Response) {
    // Once it hits this location, the kingdom will already exist on the request so we can
    // delete it and return out.
    await request.kingdom.remove();
    return response.send();
  }

  /**
   * Gets the kingdom that is bound to the selected user. The user is already located and found so
   * this process is located around returning its existing kingdom if bound or locating it.
   */
  public static async getUserKingdomByName(request: IUserRequest, response: Response) {
    // once it hits this location, the user will already exist on the request so we can
    // delete it and return out. And if the user is already containing the kingdom, just return it.
    if (!_.isNil(request.user.kingdom)) return response.json({ kingdom: request.user.kingdom });

    const userRepository = getCustomRepository(UserRepository);
    const kingdom = await userRepository.getUserKingdom(request.user);

    return response.json({ kingdom: kingdom || null });
  }

  /**
   *  Updates the given users kingdom with the provided kingdom name in the body. The user has
   *  already been located so this process focuses on locating the kingdom, binding and saving.
   */
  public static async setUserKingdomByName(request: IUserRequest, response: Response) {
    // Since a kingdom only has a name for it to be required to be found / crated, lets pull the name from
    // the body and ensure that the kingdom does already exist.
    const { name } = request.body;

    const kingdomRepository = getCustomRepository(KingdomRepository);

    // if the kingdom does not exist, then lets bail out early and ensure that we let hte user know
    // that the kingdom does not exist.
    if (!(await kingdomRepository.existsByIdentifer(name)))
      return response.status(httpCodes.BAD_REQUEST).json({
        message: `no kingdom exists by the provided identifer ${name}`
      });

    // Once it hits this location, the user will already exist on the request so we can
    // delete it and return out.
    const kingdom = await kingdomRepository.findByIdentifier(name);
    request.user.kingdom = kingdom;

    // save the user and ensure that we return back that everything went okay.
    await request.user.save();
    return response.send();
  }
}
