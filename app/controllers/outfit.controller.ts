import { Request, Response } from 'express';
import * as _ from 'lodash';

import httpCodes from '../constants/httpCodes';
import { IUserRequest } from '../requests/IRequest';

export default class OutfitsController {
  public static async getAllUserOutfits(request: IUserRequest, response: Response) {}
  public static async getUserOutfitItemByIdentifier(request: IUserRequest, response: Response) {}
  public static async deleteUserOutfitItemByIdentifier(request: IUserRequest, response: Response) {}
  public static async addOutfitsToUserOutfits(request: IUserRequest, response: Response) {}
}
